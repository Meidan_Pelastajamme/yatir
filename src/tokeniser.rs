#[derive(Debug, PartialEq)]
pub struct Tokeniser {
    pub content: Vec<char>,
    pub pos: usize,
}

impl Tokeniser {
    pub fn new(content: String) -> Self {
        Self {
            content: content.chars().collect(),
            pos: 0
        }
    }

    fn next_token(&mut self) -> String {
        let mut token: String = Default::default();
        while self.pos < self.content.len() {
            let ch = self.content[self.pos];

            match ch {
                ch if ch.is_whitespace() => {
                    while self.pos < self.content.len() && self.content[self.pos].is_whitespace() {
                        self.pos += 1;
                    }
                }
                ch if ch.is_alphabetic() => {
                    while self.pos < self.content.len() && self.content[self.pos].is_alphabetic() {
                        token.push(self.content[self.pos]);
                        self.pos += 1;
                    }
                    return token;
                }
                ch if ch.is_numeric() => {
                    while self.pos < self.content.len() && (self.content[self.pos].is_numeric()) {
                        token.push(self.content[self.pos]);
                        self.pos += 1;
                    }
                    return token;
                }
                ch => {
                    token.push(ch);
                    self.pos += 1;
                    return token;
                }
            }
        }
        token
    }

    pub fn collect(&mut self) -> Vec<String> {
        let mut tokens: Vec<String> = Default::default();
        while self.pos < self.content.len() {
            tokens.push(self.next_token());
        }
        tokens
    }
}
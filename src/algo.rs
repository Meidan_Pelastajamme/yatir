use std::collections::HashMap;
use libm::log10;

use super::tokeniser::*;

type Corpus = HashMap<String, Doc>;
type Doc = HashMap<String, i32>;

fn token_count(tokens: Vec<String>) -> Doc {
    let mut token_count: HashMap<String, i32> = HashMap::new();
    for token in tokens {
        let count = token_count.entry(token).or_insert(0);
        *count += 1;
    }
    token_count
}

// t: Raw count (the number of times that term t occurs in document d)
fn tf(t: i32) -> f32 {
    log10(((1 + t)).into()) as f32
}
// n_d: number of documents in the corpus
// n_d_t: number of documents where the term t appears
fn idf(n_d: usize, n_d_t: usize) -> f32 {
    log10((n_d / n_d_t) as f64) as f32
}

pub fn tfidf(term: &str, doc: &Doc, corpus: &Corpus) -> f32 {
    if let Some(term_frequency) = doc.get(term).cloned() {
        tf(term_frequency) * idf(corpus.len() + 1, corpus.values().filter(|d| d.contains_key(term)).count())
    } else {
        0.0
    }
}


pub fn to_corpus(names: Vec<String>, all_tokens: Vec<Vec<String>>) -> Corpus {
    let mut corpus: Corpus = HashMap::new();
    for (name, tokens) in names.iter().zip(all_tokens.iter()) {
        corpus.insert(name.to_string(), token_count(tokens.to_vec()));
    }
    corpus
}

#[derive(Debug)]
pub struct Index{
    docs: Vec<(String, HashMap<String, f32>)>
}

impl Index {
    pub fn new() -> Self {
        Self {
            docs: Default::default()
        }
    }

    pub fn populate(&mut self, corpus: Corpus) {
        for (name, doc) in &corpus {
            let mut index_doc: HashMap<String, f32> = Default::default();
            for (term, freq) in doc {
                index_doc.insert(term.to_string(), tfidf(&term, &doc, &corpus));
            }
            self.docs.push((name.to_string(), index_doc));
        }
    }

    pub fn query(&self, query: String) -> Vec<(String, f32)> {
        let tokens = Tokeniser::new(query).collect();
        let mut result: Vec<(String, f32)> = Default::default();
        for (name, doc) in &self.docs {
            let mut score: f32 = 0.0;
            for token in &tokens {
                score += doc.get(token).unwrap_or(&0.0);
            }
            result.push((name.to_string(), score));
        }
        result.sort_by(|(_, x), (_, y)| x.partial_cmp(y).expect(&format!("{x} and {y} are not comparable")));
        result.reverse();
        result
    }

    pub fn to_json(&self) -> String {
        todo!()
    }

    pub fn from_json(&mut self) {
        todo!()
    }
}
mod tokeniser;
use crate::tokeniser::*;

mod algo;
use crate::algo::*;

fn main() {
    let input1: String = "          haha.  haha mutti mutti,    mutti mit onkel        ist 45.".to_string();
    let input2: String = "          haha.  haha papa papa papa,    papa mit onkel        ist 50.".to_string();

    let tokens1 = Tokeniser::new(input1.clone()).collect();
    let tokens2 = Tokeniser::new(input2.clone()).collect();

    let names: Vec<String> = vec!["input1".to_string(), "input2".to_string()];
    let all_tokens = vec![tokens1, tokens2];

    let corpus = to_corpus(names, all_tokens);

    let mut res: Vec<(String, f32)> = Default::default();
    for doc in &corpus {
        res.push((doc.0.to_string(), tfidf("papa", doc.1, &corpus)));
    }

    let mut index = Index::new();
    index.populate(corpus);
    let mut q = String::from("Die person, die 50 jahre alt ist");
    let mut query = index.query(q.clone());
    for i in 1..15 {
        println!("\n\n");
    }
    println!("\n\nAbfrage: {}\n", q);
    println!("Statistiken: {:?}", query);
    if query[0].0 == "input1".to_string() {
        println!("Ergebnis: {}", &input1)
    } else {
        println!("Ergebnis: {}", &input2)
    }
    for i in 1..5 {
        println!("\n\n");
    }
    let mut q = String::from("Ich habe vergessen, wie alt die Person war aber es war mein mutti");
    let mut query = index.query(q.clone());
    println!("\n\nAbfrage: {}\n", q);
    println!("Statistiken: {:?}", query);
    if query[0].0 == "input1".to_string() {
        println!("Ergebnis: {}", &input1);
    } else {
        println!("Ergebnis: {}", &input2);
    }
    for i in 1..5 {
        println!("\n\n");
    }
    const LOREM_IPSUM: &str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
}

use yatir;
use yatir::tokeniser;
use yatir::algo;

const LOREM_IPSUM: &str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

#[test]
fn tokeniser_new() {
    assert_eq!(
        tokeniser::Tokeniser::new(LOREM_IPSUM.to_string()),
        tokeniser::Tokeniser {
            content: LOREM_IPSUM.chars().collect(),
            pos: 0
        }
    )
}

#[test]
fn tokeniser_() {
    assert_eq!(
        tokeniser::Tokeniser::new(LOREM_IPSUM.to_string()).collect(),
        vec!["Lorem", "ipsum", "dolor", "sit", "amet", ",", "consectetur", "adipiscing", "elit", ",", "sed", "do", "eiusmod", "tempor", "incididunt", "ut", "labore", "et", "dolore", "magna", "aliqua", "."]
    )
}

